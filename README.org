#+title:Structured Light 3d Scanner
This project aims to reproduce the paper cited below, achieving 3d reconstruction by a single shot in a non-calibrated camera-projector configuration.

This is a WIP: while all the functionalities of feature matching described in paper are implemented, affine and metric reconstruction are still missing.

* Pattern Design
Pattern design is achieved with galois, a python package which extends numpy to operate on Galois Fields. In pattern folder one can find the code for pattern generation.
[[../pattern/pattern.png]]
* Feature Extraction
Feature points extraction is achieved 
 * detector.py
library functions implementing feature detectors described in paper below.
  - Cross-Shaped Convolution Mask
  - Twofold-Symmetry Rotation Detector
 * grid_points.py
class representing grid points found in the pattern, containing all functionalities to compute type and codeword for a given point.
 * feature_extractor.py
code for performing feature extraction, threshold and window size are parameters adjusted by the user. There's also a horrible GUI to have a visual feedback exploring parameters. 

* Feature Matching
 * points_matching.py
some rudimental outliers detection and a sketch for an iterative procedure to remove feature's ambiguity.
 * coupling.py
points get aligned in two vectors ready for fundamental matrix estimation

* TODO Reconstruction
The reconstruction obtained is up to a projective transformation. It needs to be implemented the stratified reconstruction (affine, euclidean) as described in Hartley et al.

* TODO Robustness Test
some test on feature detection and codeword computation still need to be done (test with different cameras, objects,...)

* Results
 * example picture
   [[./main/imgbkp/non-coplanar1.jpg]]
 * detected gridpoints
   [[./scene_data/non-coplanar1_real_gridpoints.png]]
 * detected gridcouples - /type-one/ and /type-two/ points
   [[./scene_data/non-coplanar1_t1t2_real_match.png]]
 * reconstructed cloud-point
   [[./scene_data/pointcloud.jpg]]



@article{song2009determining,
  title={Determining both surface position and orientation in structured-light-based sensing},
  author={Song, Zhan and Chung, Ronald},
  journal={IEEE Transactions on Pattern Analysis and Machine Intelligence},
  volume={32},
  number={10},
  pages={1770--1780},
  year={2009},
  publisher={IEEE}
}

