import galois
import pygame
import numpy as np
from functools import reduce

class RandomMatrixGenerator:
    def __init__(self, elems, coeffs, cols, rows):
        self.field = galois.GF(elems)
        self.poly = galois.Poly(coeffs,self.field,"desc")
        self.lfsr = galois.FLFSR(self.poly)
        self.randomseq = iter([i.item() for i in self.lfsr.step(cols*rows)])
        A = np.zeros((rows, cols), dtype="uint8")
        for k in range(rows):
            for i in range(cols):
                for j in range(rows):
                    if i==j:
                        A[(j+k*3)%rows][i] = next(self.randomseq)
        self.random_matrix = A
        # self.codewords = np.array([[reduce(lambda x,y: np.concatenate((x,y),0), [row[j:j+3] for row in matrix[i:i+2]]) for j in range(len(A[0]-2))] for i in range(len(A)-1)])
        self.codewords = np.array([[reduce(lambda x,y : np.concatenate((x,y), 0), [row[i:i+3] for row in A[j:j+2]])  for
                                    i in range(len(A[0])-2)] for j in range(len(A)-1)])
        print(self.codewords.shape)
        print(self.random_matrix.shape)

    def getmatrix(self):
        return self.random_matrix

    def printmatrix(self):
        [print(i) for i in self.random_matrix]

    def testunicityv1(self):
        codewords = self.codewords.reshape(-1,6)
        for i in range(len(codewords)):
            for j in range(len(codewords)):
                if i!=j and all(codewords[i]==codewords[j]):
                    print(codewords[j], (i,j))
    def testunicity(self):
        codewords = self.codewords
        for k in range(len(codewords)):
            for v in range(len(codewords[0])):
                for i in range(len(codewords)):
                    for j in range(len(codewords[0])):
                        if i!= k and v != j and all(codewords[k][v] == codewords[i][j]): 
                            print(codewords[i][j], (i,j), (k,v))
                    
