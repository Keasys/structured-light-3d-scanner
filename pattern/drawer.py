import pygame
import time
import pickle

import pattern_generator as pg

import numpy as np
from functools import reduce

red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
white = (255,255,255)
black = (0,0,0)

def drawElement(color, empty=False):
    def getElementPoints(i,j,side):
        offset = (absoluteside - side)
        #left , top, right, bottom 
        return [((i*side)+offset*(i+1), (side/2+(j*side))+(offset/2)*(j*2+1)),
                ((side/2+(i*side)+((offset)/2)*((i*2+1))),(j*side)+offset*(j+1)),
                ((side+(i*side))+offset*i,(side/2+(j*side))+(offset/2)*(j*2+1)),
                ((side/2+(i*side))+((offset)/2)*((i*2+1)), ((side+(j*side))+offset*j))]

    def inner(i, j):
        vertices = getElementPoints(i,j,absoluteside)
        pygame.draw.polygon(screen,color,vertices)    
        if empty:
            vertices = getElementPoints(i,j,absoluteside*0.75)
            pygame.draw.polygon(screen,black,vertices)
    return inner


pattern = {
    0: drawElement(red),
    1: drawElement(green),
    2: drawElement(blue),
    3: drawElement(black),
    4: drawElement(red, empty=True),
    5: drawElement(green, empty=True),
    6: drawElement(blue, empty=True),
    7: drawElement(black, empty=True)
}

M=1024

pygame.init()

W= M*0.988 #-(M/64)*0.4
H= M+(M/64)*1.2
screen = pygame.display.set_mode((W,H))




N=64
absoluteside = M/N
screen.fill(white)

matrix_gen = pg.RandomMatrixGenerator(4,[2,2,1,3,2,2,1], 63, 65)
A = matrix_gen.getmatrix()
P = matrix_gen.codewords

matrix_gen.testunicity()


while True:
    screen.fill(white)
    [[pattern[A[i][j]](j,i) for i in range(65)] for j in range(63)]
    pygame.image.save(screen, "pattern_test.png")
    pygame.display.flip()
    break



codewords = {str(matrix_gen.codewords[i][j])[1::2]: (i,j) for i in range(len(matrix_gen.codewords)) for j in range(len(matrix_gen.codewords[0]))}





with open("codewords.dump", "wb") as pk:
    pickle.dump(codewords, pk)

with open("pattern.dump", "wb") as pk:
    pickle.dump(A, pk)

with open("P.dump", "wb") as pk:
    pickle.dump(P, pk)

pygame.display.quit()
