import os
import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv
from math import sqrt
from sys import exit

from itertools import product

def getManhattanDistance(compare):
    def wrapped(x, ref="p1top2"):
        p,q = x, compare
        p_x, p_y = p.position
        q_x, q_y = q.position
        manhattan = p_x - q_x,  p_y - q_y if ref == "p1top2" else q_x - p_x,  q_y - p_y 
        return 5000 if manhattan[0] < 0 or manhattan[1] < 0 else manhattan[0] + manhattan[1]
    return wrapped


def getDistance(compare):
    def wrapped(x):
        return sqrt(sum([ (x.position[i] - compare.position[i])**2 for i in range(len(x.position))]))
    return wrapped



class GridPoint():   
    def __init__(self, position, wsize, img):
        self.position = position
        self.normalizeCoordinates(img)
        self.wsize = wsize
        self.computeType(img)
        self.codeword = []
        self.sx = []
        self.dx = []
    
    def getColorRGB(self, rgb, t):
        # if all([i>t for i in rgb]):
        #     return "white"
        if all([i<t for i in rgb]):
            return "black"
        else:
            if rgb[0] > t : return   "red"
            elif rgb[1] > t:return  "green"
            elif rgb[2] > t: return  "blue"

    def findSx(self, points):
        dist = getDistance(self)
        x,y = self.position
        row = [p for p in points if (p.position[1]) >= y-self.wsize*2 and p.position[1] <= y+self.wsize*2 ]
        # debug_image = cv.imread("./non-coplanar_real_gridpoints.png")
        # cv.line(debug_image, pt1=(0,y+self.wsize*2), pt2=(debug_image.shape[0], y+ self.wsize*2), color=(255,255,0),thickness=1)
        # cv.line(debug_image, pt1=(0,y-self.wsize*2), pt2=(debug_image.shape[0], y- self.wsize*2), color=(255,255,0),thickness=1)
        
        for p in row:
            px,py = p.position
            # debug_image = cv.circle(debug_image, (px,py), 1, (125,0,220), 1)
             
        sx = [i for i in row if i.position[0] < x]
        dist = getDistance(self)
        if len(sx) != 0 :
            sx.sort(key=getDistance(self))
            
            if len(sx) > 1 and dist(sx[1]) > 1.5*dist(sx[0]):
                candidate_sx = [sx[0]]
            else:
                candidate_sx = sx[:2]
            self.sx = candidate_sx
        else: candidate_sx = []
    
    def findDx(self, points):
        dist = getDistance(self)
        x,y = self.position
        row = [p for p in points if (p.position[1]) >= y-self.wsize*2 and p.position[1] <= y+self.wsize*2 ]
        # debug_image = cv.imread("./non-coplanar_real_gridpoints.png")
        # cv.line(debug_image, pt1=(0,y+self.wsize*2), pt2=(debug_image.shape[0], y+ self.wsize*2), color=(255,255,0),thickness=1)
        # cv.line(debug_image, pt1=(0,y-self.wsize*2), pt2=(debug_image.shape[0], y- self.wsize*2), color=(255,255,0),thickness=1)
        
        for p in row:
            px,py = p.position
            # debug_image = cv.circle(debug_image, (px,py), 1, (125,0,220), 1)

        dx = [i for i in row if i.position[0] > x]
        if len(dx) != 0:
            dx.sort(key=getDistance(self))
            if len(dx) > 1 and dist(dx[1]) > 1.5*dist(dx[0]):
                candidate_dx = [dx[0]]
            else:
                candidate_dx = dx[:2]
            self.dx = candidate_dx
        else: candidate_dx = [] 
            
    def getColorBGR(self, rgb, t):
        # if all([i>t for i in rgb]):
        #     return "white"
        r,g,b = rgb
        if all([i<t for i in rgb]):
            return 3
        else:
            if r > g and r > b : return  2
            elif g > r and g > b:return  1
            elif b > r and b > g: return  0
            else: return 9

    def normalizeCoordinates(self, img):
        norm_x = self.position[0] / (img.shape[0]) # - 0.5
        norm_y = self.position[1] / (img.shape[1]) # - 0.5
        self.normalizedCoordinates = norm_x, norm_y
        
        
    def computeType(self, img):
        x,y = self.position
        horizontal = np.array([sum(img[y][x+i])/3 for i in range(-self.wsize, self.wsize)])
        vertical = np.array([sum(img[y+i][x])/3 for i in range(-self.wsize, self.wsize)])
        self.horizontal = np.mean(horizontal)
        self.vertical = np.mean(vertical)
        horizontal_sx = horizontal[:len(horizontal)//2]
        horizontal_dx = horizontal[len(horizontal)//2+1:]


        conditions = self.horizontal > self.vertical
        
        if conditions:# [0] and conditions[-1]:
            self.type = 0
        else:
            self.type = 1
    def computeCodeWordProjectorPattern(self, points,img):
        x,y = self.position
        row = [p for p in points if p.position[1] == y ]
        debug_image = cv.imread("../pattern/pattern_test.png", 0)
        debug_image = cv.cvtColor(debug_image, cv.COLOR_GRAY2BGR)
        cv.line(debug_image, pt1=(0,y+self.wsize*2), pt2=(debug_image.shape[0], y+ self.wsize*2), color=(255,255,0),thickness=1)
        cv.line(debug_image, pt1=(0,y-self.wsize*2), pt2=(debug_image.shape[0], y- self.wsize*2), color=(255,255,0),thickness=1)
        
        for p in row:
            px,py = p.position
            debug_image = cv.circle(debug_image, (px,py), 1, (125,0,220), 1)
             
        
        sx = [i for i in row if i.position[0] < x]
        dx = [i for i in row if i.position[0] > x]

        for s in sx:
            s_x, s_y = s.position
            debug_image = cv.circle(debug_image, (s_x,s_y), 1, (255,0,255), 1)
        for d in dx:
            d_x, d_y = d.position
            debug_image = cv.circle(debug_image, (d_x, d_y), 1, (0,0,255), 1)
        
        if len(sx) != 0 and len(dx) != 0:
            candidate_sx = min([s for s in sx], key=lambda elem: abs(x-elem.position[0]))
            candidate_dx = min([d for d in dx], key=lambda elem: abs(x-elem.position[0]))
                
        else:
            print("exception")
            self.codeword = "border"
            return

        dist = getDistance(self)
        
        color = (0,0,255) if dist(candidate_sx) > 2*dist(candidate_dx) or dist(candidate_dx) > 2*dist(candidate_sx) else (225,120,0)
        
        debug_image = cv.circle(debug_image, (x,y), 5, color, -1)
        debug_image = cv.circle(debug_image, candidate_dx.position, 5, (225,0,255), -1)
        debug_image = cv.circle(debug_image, candidate_sx.position, 5, (225,0,255), -1)
        
        cv.imwrite(f"./debug_original/{x}x{y}.png", debug_image)
        sx_x, sx_y = candidate_sx.position
        dx_x, dx_y = candidate_dx.position
        four = str(self.getColorBGR(img[sx_y+self.wsize][sx_x],120))
        one = str(self.getColorBGR(img[sx_y-self.wsize][sx_x],120))
        five = str(self.getColorBGR(img[y+self.wsize][x], 120))
        two = str(self.getColorBGR(img[y-self.wsize][x], 120))
        six = str(self.getColorBGR(img[dx_y+self.wsize][dx_x], 120))
        three = str(self.getColorBGR(img[dx_y-self.wsize][dx_x], 120))

        debug_color = cv.imread("./img/pattern.png")
        
        debug_color = cv.circle(debug_color, (x,y), 5, color, -1)
        debug_color = cv.circle(debug_color, candidate_dx.position, 5, (225,0,255), -1)
        debug_color = cv.circle(debug_color, candidate_sx.position, 5, (225,0,255), -1)
        
        debug_color = cv.circle(debug_color, (sx_x, sx_y+self.wsize), 3, (225,0,255), -1)
        debug_color = cv.circle(debug_color, (sx_x, sx_y-self.wsize), 3, (225,0,255), 1)
        debug_color = cv.circle(debug_color, (x, y+self.wsize), 3, (225,0,255), -1)
        debug_color = cv.circle(debug_color, (x, y-self.wsize), 3, (225,0,255), 1)
        debug_color = cv.circle(debug_color, (dx_x, dx_y+self.wsize), 3, (225,0,255), -1)
        debug_color = cv.circle(debug_color, (dx_x, dx_y-self.wsize), 3, (225,0,255), 1)
        self.codeword = one+two+three+four+five+six
        try:
            cv.imwrite(f"./debug_original/{x}x{y}_{one+two+three+four+five+six}.png", debug_color)
        except Exception as e:
            print(e)

        
    def computeCodeWord(self,points,img):
        x,y = self.position
        self.findDx(points)
        self.findSx(points)
        dist = getDistance(self)
        
        if len(self.dx) == 0 or len(self.sx) == 0:
            print("exception")
            self.codeword = "border"
            return

        
        if dist(self.sx[0]) > 1.5*dist(self.dx[0]) or dist(self.dx[0]) > 1.5*dist(self.sx[0]):
            print("distances error")
            self.codeword = "error"
            return
        
        try:
            five = self.getColorBGR(img[y+self.wsize*2][x], 180)
            two = self.getColorBGR(img[y-self.wsize*2][x], 180)
            cw_center = np.array([two, five]).reshape(2,-1)
            
        except IndexError as ie:
            print("index error at center")
            self.codeword = "error"
            print(x,y)
            return
        
        
        abort = False
        cw_sx = []
        for i in range(len(self.sx)):
            if abort: break
        
            sx_x, sx_y = self.sx[i].position
            try:
                four = self.getColorBGR(img[sx_y+self.wsize*2][sx_x],180)
                one = self.getColorBGR(img[sx_y-self.wsize*2][sx_x],180)
                cw_sx.append(np.array([one, four]).reshape(2,-1))
            except IndexError as ie:
                print("index error at sx")
                print(x,y)
            try:
                codeword[0].append(one)
                codeword[3].append(four)
            except:
                abort = True
                self.codeword.append("border")
                codeword = "border"
                print(x,y," border")
                break
        partial_codewords = [np.concatenate((i, cw_center), 1) for i in cw_sx]
        cw_dx = []
        for i in range(len(self.dx)):
            dx_x, dx_y = self.dx[i].position
            try:
                six = self.getColorBGR(img[dx_y+self.wsize*2][dx_x], 180)
                three = self.getColorBGR(img[dx_y-self.wsize*2][dx_x], 180)
                cw_dx.append(np.array([three,six]).reshape(2,-1))
            except IndexError as ie:
                print("index error dx")
                print(x,y)
            try:
                codeword[5].append(six)
                codeword[2].append(three)
                
                    
            except:
                abort = True
                self.codeword.append("border")
                codeword = "border"
                print(x,y, "border")
                break
            pass


        codewords = [np.concatenate((i,j), 1) for i in partial_codewords for j in cw_dx]
        codewords = [str(i.reshape(-1,6))[::2][1:-1] for i in codewords]
        self.codeword = codewords
        

    def VisualizeCodeword(self, k):
        debug_color = cv.imread("./img/non-coplanar1.jpg")
        debug_image = cv.imread("./non-coplanar1_real_gridpoints.png")
        x,y = self.position
        color =  (255,0,0) if self.codeword != "error" and self.codeword != "border" else (0,0,255)
        try:
            with open("./debug", "r") as pk:
                x = pickle.load(pk)
        except FileNotFoundError as fnf:
            os.system("mkdir debug")
        except IsADirectoryError as de:
            pass
        debug_image = cv.circle(debug_image, (x,y), 5, color, -1)
        debug_color = cv.circle(debug_color, (x,y), 5, color, -1)
        debug_color = cv.circle(debug_color, (x, y+self.wsize), 3, (225,0,255), -1)
        debug_color = cv.circle(debug_color, (x, y-self.wsize), 3, (225,0,255), 1)
        for i in self.dx:
            dx_x, dx_y = i.position
            debug_color = cv.circle(debug_color, i.position, 5, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (dx_x, dx_y+self.wsize), 3, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (dx_x, dx_y-self.wsize), 3, (225,0,255), 1)
            debug_image = cv.circle(debug_image, i.position, 5, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (dx_x, dx_y+self.wsize), 3, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (dx_x, dx_y-self.wsize), 3, (225,0,255), 1)
        for i in self.sx:
            sx_x, sx_y = i.position
            debug_color = cv.circle(debug_color, i.position, 5, (225,0,255), -1)       
            debug_color = cv.circle(debug_color, (sx_x, sx_y+self.wsize), 3, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (sx_x, sx_y-self.wsize), 3, (225,0,255), 1)
            debug_image = cv.circle(debug_image, i.position, 5, (225,0,255), -1)       
            debug_image = cv.circle(debug_image, (sx_x, sx_y+self.wsize), 3, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (sx_x, sx_y-self.wsize), 3, (225,0,255), 1)
        
            
        os.system(f"mkdir ./debug/{k}")
        cv.imwrite(f"./debug/{k}/{x}x{y}_scene.png", debug_image)
        cv.imwrite(f"./debug/{k}/{x}x{y}_{'-'.join(self.codeword)}_scene.png", debug_color)

    def VisualizeFinalCodeword(self, filename):
        debug_color = cv.imread("./img/non-coplanar1.jpg")
        debug_image = cv.imread("./non-coplanar1_real_gridpoints.png")
        x,y = self.position
        color =  (255,0,0) if self.codeword != "error" and self.codeword != "border" else (0,0,255)
        try:
            with open("./debug", "r") as pk:
                x = pickle.load(pk)
        except FileNotFoundError as fnf:
            os.system("mkdir debug")
        except IsADirectoryError as de:
            pass
        debug_image = cv.circle(debug_image, (x,y), 5, color, -1)
        debug_color = cv.circle(debug_color, (x,y), 5, color, -1)
        debug_color = cv.circle(debug_color, (x, y+self.wsize), 3, (225,0,255), -1)
        debug_color = cv.circle(debug_color, (x, y-self.wsize), 3, (225,0,255), 1)
        for i in self.dx:
            dx_x, dx_y = i.position
            debug_color = cv.circle(debug_color, i.position, 5, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (dx_x, dx_y+self.wsize), 3, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (dx_x, dx_y-self.wsize), 3, (225,0,255), 1)
            debug_image = cv.circle(debug_image, i.position, 5, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (dx_x, dx_y+self.wsize), 3, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (dx_x, dx_y-self.wsize), 3, (225,0,255), 1)
        for i in self.sx:
            sx_x, sx_y = i.position
            debug_color = cv.circle(debug_color, i.position, 5, (225,0,255), -1)       
            debug_color = cv.circle(debug_color, (sx_x, sx_y+self.wsize), 3, (225,0,255), -1)
            debug_color = cv.circle(debug_color, (sx_x, sx_y-self.wsize), 3, (225,0,255), 1)
            debug_image = cv.circle(debug_image, i.position, 5, (225,0,255), -1)       
            debug_image = cv.circle(debug_image, (sx_x, sx_y+self.wsize), 3, (225,0,255), -1)
            debug_image = cv.circle(debug_image, (sx_x, sx_y-self.wsize), 3, (225,0,255), 1)
        
        
        cv.imwrite(f"./debug/{x}x{y}.png", debug_image)
        cv.imwrite(f"./debug/{x}x{y}_{filename}.png", debug_color)

        
        
    def Visualize(self, half_window, img):
        print(self.position)
        x,y = self.position
        plt.figure()
        plt.imshow([[img[y+j][x+i] for i in range(-half_window, half_window)] for j in range(-half_window, half_window)])    
        plt.show()

    def Confront(self, half_window, img):
        x,y = self.position
        fig = plt.figure()
        p1 = fig.add_subplot(121)
        p1.imshow([[img[y+j][x+i] for i in range(-half_window, half_window)] for j in range(-half_window, half_window)])
        p2 = fig.add_subplot(122)
        p2.imshow(img)
        plt.show()
    

    def __repr__(self):
        return str(self.position)

    def __eq__(self, other):
        return self.codeword == other.codeword  and self.type == other.type
        
