import random
import pickle
import cv2 as cv
import grid_points 
import sys

import numpy as np
from numpy.linalg import svd

import matplotlib.pyplot as plt
import open3d as o3d

def skewSymmetric(v):
    x,y,z = v.squeeze()
    print(x)
    return np.array([[0,-z,y],
                     [z,0,-x],
                     [-y,x,0]])

    


def nullspace(A, atol=1e-13, rtol=0):
    """Compute an approximate basis for the nullspace of A.

    The algorithm used by this function is based on the singular value
    decomposition of `A`.

    Parameters
    ----------
    A : ndarray
        A should be at most 2-D.  A 1-D array with length k will be treated
        as a 2-D with shape (1, k)
    atol : float
        The absolute tolerance for a zero singular value.  Singular values
        smaller than `atol` are considered to be zero.
    rtol : float
        The relative tolerance.  Singular values less than rtol*smax are
        considered to be zero, where smax is the largest singular value.

    If both `atol` and `rtol` are positive, the combined tolerance is the
    maximum of the two; that is::
        tol = max(atol, rtol * smax)
    Singular values smaller than `tol` are considered to be zero.

    Return value
    ------------
    ns : ndarray
        If `A` is an array with shape (m, k), then `ns` will be an array
        with shape (k, n), where n is the estimated dimension of the
        nullspace of `A`.  The columns of `ns` are a basis for the
        nullspace; each element in numpy.dot(A, ns) will be approximately
        zero.
    """

    A = np.atleast_2d(A)
    u, s, vh = svd(A)
    tol = max(atol, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns


src = sys.argv[1]
name = src.split("/")[-1][:-4]

projector = cv.imread("./imgbkp/pattern.png", 0)
scene = cv.imread(src, 0)

projector_height , projector_width = projector.shape
scene_height, scene_width = scene.shape

with open(f"./{name}_points_matched.dump", "rb") as pk:
    scene_coordinates=pickle.load(pk)

with open(f"./{name}_projector_points_matched.dump", "rb") as pk:
    projector_coordinates = pickle.load(pk)


assert len(scene_coordinates) == len(projector_coordinates)



scene_coordinates = np.array([np.array(i.position) for i in scene_coordinates])
projector_coordinates = np.array([np.array(i.position) for i in projector_coordinates])




F, mask = cv.findFundamentalMat(projector_coordinates, scene_coordinates, cv.FM_8POINT)





projector = cv.imread("./imgbkp/pattern.png", 0)
scene = cv.imread(src, 0)

R,c = projector.shape

projector_bw = cv.cvtColor(projector, cv.COLOR_GRAY2BGR)
scene_bw = cv.cvtColor(scene, cv.COLOR_GRAY2BGR)

projector_lines = cv.computeCorrespondEpilines(scene_coordinates.reshape(-1,1,2), 2, F)
projector_lines = projector_lines.reshape(-1,3)

for r,p1,p2 in zip(projector_lines,projector_coordinates, scene_coordinates):
    color = tuple(np.random.randint(0,255,3).tolist())
    x0,y0 = map(int, [0, -r[2]/r[1] ])
    x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
    projector_bw = cv.line(projector_bw, (x0,y0), (x1,y1), color, 1)
    projector_bw = cv.circle(projector_bw, ((int((p1[0]))), int((p1[1]))), 5, color, -1)
    

plt.figure()
plt.imshow(projector_bw)
plt.show()

scene_lines = cv.computeCorrespondEpilines(projector_coordinates.reshape(-1,1,2), 1, F)
scene_lines = scene_lines.reshape(-1,3)

R,c = scene.shape
for r,p1 in zip(scene_lines,scene_coordinates):
    color = tuple(np.random.randint(0,255,3).tolist())
    x0,y0 = map(int, [0, (-r[2]/r[1]) ])
    x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
    scene_bw = cv.line(scene_bw, (x0,y0), (x1,y1), color, 1)
    scene_bw = cv.circle(scene_bw, ((int((p1[0]))), int((p1[1]))), 5, color,-1)

len(projector_coordinates)
plt.figure()
plt.imshow(scene_bw)
plt.show()




e = nullspace(F)
e_prime = nullspace(F.T)

e_x = skewSymmetric(e)

P = np.concatenate((np.identity(3), np.zeros((1,3)).T), axis=1)

e_prime_x = skewSymmetric(e_prime)
my_P_prime = np.concatenate((np.matmul(e_prime_x, F)+np.matmul(e_prime, np.random.rand(1,3)*13),(random.random()*23)*e_prime), axis=1)
P, P_prime = cv.sfm.projectionsFromFundamental(F)

scene_coordinates=np.array([i-np.array([scene_width//2, scene_height//2]) for i in scene_coordinates])
projector_coordinates=np.array([i-np.array([projector_width//2, projector_height//2]) for i in projector_coordinates])

normalized_scene_coordinates = np.array([i/np.array([scene_height//2, scene_width//2]) for i in scene_coordinates])
normalized_projector_coordinates = np.array([i/np.array([projector_height//2, projector_width//2]) for i in projector_coordinates])


# normalized_scene_coordinates = np.array([cv.mat_wrapper.Mat(i) for i in normalized_scene_coordinates])
# normalized_projector_coordinates = np.array([cv.mat_wrapper.Mat(i) for i in normalized_projector_coordinates])

K = np.array([[0.9,0,1], [0,0.9,1], [0,0,1]])

# assert False
# K= cv.mat_wrapper.Mat(np.array([[0.9,0,1], [0,0.9,1], [0,0,1]]))


# output = cv.sfm.reconstruct([normalized_scene_coordinates.T, normalized_projector_coordinates.T], K, is_projective=True)
 
fourD_points=cv.triangulatePoints( P, my_P_prime, normalized_scene_coordinates.T, normalized_projector_coordinates.T).T



# 
# 
# print(fourD_points)
# fourD_points.shape
# 
# 
threeD_points = fourD_points/fourD_points[3]
threeD_points = np.delete(threeD_points, (3), axis=1)


print(threeD_points)
print(threeD_points.shape)




 
pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(threeD_points)
o3d.io.write_point_cloud("./pointcloud.ply", pcd)
 
o3d.visualization.draw_geometries([pcd])

