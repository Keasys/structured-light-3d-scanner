import random
import cv2 as cv
import matplotlib.pyplot as plt
from grid_points import GridPoint
import pickle
import sys

src = sys.argv[1]
name = src.split("/")[-1][:-4]

with open("../projector_data/projector_point_codewords.dump", "rb") as pk:
    projector_couples = pickle.load(pk)

with open(f"./{name}_codeword.dump", "rb") as pk:
    scene_couples = pickle.load(pk)




projector_couples=[i for  i in projector_couples if i[0].codeword != "border" and i[0].codeword != "error"]



projector_points = []
scene_points = []

for type1_scene, type2_scene in scene_couples:
    for type1_projector, type2_projector in projector_couples:
        if type1_scene.codeword == type1_projector.codeword:
            projector_points.append(type1_projector)
            scene_points.append(type1_scene)
        if type2_scene.codeword == type2_projector.codeword:
            projector_points.append(type2_projector)
            scene_points.append(type2_scene)
            
            

with open("matched_points.csv", "w") as fp:
    fp.write(f"codeword, type, x_scene, y_scene, x_proj, y_proj\n")
    for i in range(len(scene_points)):
        fp.write(f"{scene_points[i].codeword},{scene_points[i].type},{scene_points[i].position[0]},{scene_points[i].position[1]},{projector_points[i].position[0]},{projector_points[i].position[1]}\n")


with open(f"{name}_projector_points_matched.dump", "wb") as pk:
    pickle.dump(projector_points, pk)

with open(f"{name}_points_matched.dump", "wb") as pk:
    pickle.dump(scene_points, pk)



