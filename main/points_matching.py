import glob
import pickle

from grid_points import GridPoint
import cv2 as cv
import pickle
from math import exp

from time import time
from detector import *

import random
import sys


import tkinter as tk
from tkinter import ttk, Canvas
import cv2
from PIL import Image, ImageTk

src = sys.argv[1]
name = src.split("/")[-1][:-4]

original = cv.imread(src)
gray = cv.cvtColor(original, cv.COLOR_BGR2GRAY)

def getManhattanDistance(x, ref="p1top2"):
    p,q = x
    p_x, p_y = p.position
    q_x, q_y = q.position
    manhattan = p_x - q_x,  p_y - q_y if ref == "p1top2" else q_x - p_x,  q_y - p_y 
    return 5000 if manhattan[0] < 0 or manhattan[1] < 0 else manhattan[0] + manhattan[1]



def isOutlier(point):
    if point.type == 0:
        if np.mean(point.horizontal) > 174:
            return False
        return True
    if point.type == 1:
        if np.mean(point.vertical) < 227 or np.mean(point.horizontal) > 72:
            return False
        return True


def onPointsDivide():
    ''''''
    global displayed_image
    for p1 in type_one_points:
        x,y = p1.position
        vv = np.mean(p1.vertical)
        hh = np.mean(p1.horizontal)
        color = (0,255,0) if vv < 227  else (0,0,255)
        color = (0,255,0) if hh > 174 else (0,0,255)
        displayed_image = cv.circle(displayed_image, (x,y), 1, color,1)
    for p1 in type_two_points:
        x,y = p1.position
        vv = np.mean(p1.vertical)
        hh = np.mean(p1.horizontal)
        color = (255,0,0) if  vv < 227 or hh > 72 else (0,0,255)
        displayed_image = cv.circle(displayed_image, (x,y), 1, color,1)
    print(displayed_image.shape)
    img = Image.fromarray(displayed_image)
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def onCheckOutliers():
    global displayed_original, type_one_points, type_two_points
    type_one_points = [p for p in type_one_points if not isOutlier(p)]
    type_two_points = [p for p in type_two_points if not isOutlier(p)]
    print(len(type_one_points), len(type_two_points))
    for p in type_one_points:
        x,y = p.position
        color = (0,255,0)
        displayed_original = cv.circle(displayed_original, (x,y), 1, color, 1)
    for p in type_two_points:
        x,y = p.position
        color = (255,0,0)
        displayed_original = cv.circle(displayed_original, (x,y), 1, color, 1)
    print(displayed_original.shape)
    img = Image.fromarray(displayed_original)
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def onClick(event):
    print("clicked: ", event.x, event.y)
    
def onRelease(event):
    global displayed_image
    print("released: ", event.x, event.y)
    print(displayed_image.shape)
    for x,y in indexes.keys():
        if x == event.x:
            print((x,y))
            displayed_image = cv.circle(displayed_image, (x,y), 5, (0,0,255), -1)
    img = Image.fromarray(displayed_image)
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk
    
with open(f"./{name}_indexes.dump", "rb") as pk:
    indexes = pickle.load(pk)
print(len(indexes))

gridpoints = [GridPoint(i,8, original) for i in indexes.keys()]

type_one_points = [p for p in gridpoints if p.type==0]
type_two_points = [p for p in gridpoints if p.type==1]
print(len(gridpoints))

displayed_image = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)
displayed_original = cv.cvtColor(gray, cv.COLOR_GRAY2BGR)

print(displayed_image.shape)
height, width = displayed_image.shape[:2]

root = tk.Tk()
root.geometry(f"{width}x{height}")

lmain = tk.Label(root)
root.columnconfigure(0,weight=1)
root.columnconfigure(1,weight=1)
root.columnconfigure(2,weight=1)
root.columnconfigure(3,weight=3)

frame = tk.Canvas(root, width=width//2, height=height//2, scrollregion=(0,0,500, height))
frame.bind( "<Button1-ButtonPress>", onClick )
frame.bind( "<Button1-ButtonRelease>", onRelease )
img = Image.fromarray(displayed_image)
imgtk = ImageTk.PhotoImage(image=img)
image_frame = frame.create_image(width//3, height//6, anchor="n", image= imgtk)
scroll_bar = tk.Scrollbar(root)
scroll_bar.config( command = frame.yview )
frame.config( yscrollcommand=scroll_bar.set )


first_button = ttk.Button(
    root, 
    text="Divide Points Type", 
    command=onPointsDivide
)
second_button = ttk.Button(
    root, 
    text="Outliers", 
    command= onCheckOutliers
)
third_button = ttk.Button(
    root,
    text="Local Maxima",
    command=lambda x : print(x)
)

frame.grid(
    column=3,
    row=0,
    rowspan=12,
    sticky='w'
    
)

scroll_bar.grid(
    column=4,
    row=0,
    rowspan=12
)

first_button.grid(
    column=2,
    row=1
)

second_button.grid(
    column=2,
    row=2
)

third_button.grid(
    column=2,
    row=3
)




root.mainloop()




y = [np.mean(p.vertical) for p in gridpoints]
x = [np.mean(p.horizontal) for p in gridpoints]
c = [(0,0,1) if np.mean(p.vertical) > 227 or (np.mean(p.horizontal) < 175 and p.type == 0) or (np.mean(p.horizontal) < 73 and p.type==1) else  (0,1,0) if p.type == 0 else (1,0,0) for p in gridpoints]

plt.figure()
plt.scatter(x,y,c=c, s=0.8)
plt.show()



cv.imwrite(f"{name}_outliers.png", displayed_image)
cv.imwrite(f"{name}_outliers_out.png", displayed_original)


gridcouples = [min([(p,q) for q in type_two_points], key=getManhattanDistance) for p in type_one_points]


real_gridcouples = []

for gp in gridpoints:
    coupled = [(p,q) for p,q in gridcouples if q.position == gp.position]
    print(len(coupled))
    if len(coupled) > 1:
        real_gridcouples.append(min(coupled, key=getManhattanDistance))
    elif len(coupled) == 1:
        real_gridcouples.append(coupled[0])


distances = [getManhattanDistance(i) for i in real_gridcouples]

real_gridcouples = [real_gridcouples[i] for i in range(len(real_gridcouples)) if distances[i] <= np.mean(distances)*1.4]


original = cv.imread(src, 0)
original = cv.cvtColor(original, cv.COLOR_GRAY2BGR)
for p,q in gridcouples:
    cv.line(original, pt1=p.position, pt2=q.position, color=(255,170,0),thickness=1)
for p,q in real_gridcouples:
    cv.line(original, pt1=p.position, pt2=q.position, color=(0,170,255),thickness=1)

cv.imwrite(f"{name}_t1t2_match.png", original)

original = cv.imread(src)
for p,q in real_gridcouples:
    cv.line(original, pt1=p.position, pt2=q.position, color=(0,255,255),thickness=1)

cv.imwrite(f"{name}_t1t2_real_match.png", original)

original = cv.imread(src, 0)
original = cv.cvtColor(original, cv.COLOR_GRAY2BGR)
for p,q in real_gridcouples:
    cv.circle(original, p.position, 1,(0,255,0), 1)
    cv.circle(original, q.position, 1,(255,0,0), 1)

cv.imwrite(f"{name}_real_gridpoints.png", original)






original = cv.imread(src)
for p,q in real_gridcouples:
    # p.computeCodeWordOriginalPattern(type_one_points, original)
    p.computeCodeWord(type_one_points, original)
    # p.VisualizeCodeword()
    q.codeword = p.codeword
    
detected_gridcouples = [i for i in real_gridcouples if i[0].codeword != "border" and i[0].codeword != "error"]








with open("../pattern/codewords.dump", "rb") as pk:
    codewords = pickle.load(pk)

with open("../pattern/P.dump" , "rb") as pk:
    P = pickle.load(pk)


#dictionary mapping points to the corresponding positions in the pattern matrix

pos2codeword = {(i,j): [] for i in range(64) for j in range(62)}
counters = {}


#procedure to append grid points to patternmatrix I may have more points mapping to same codeword
for i in detected_gridcouples:
    counters[i[0].position] = 0
    print(f"detected codewords for {i[0]}: ", len(i[0].codeword))
    for j in codewords.keys():
        if j in i[0].codeword:
            print("-", codewords[j], j)
            pos2codeword[codewords[j]].append(i[0])
            counters[i[0].position]+=1

indecision = {}
            
posfilter =  {k:v for k,v in pos2codeword.items()}


#procedure to filter out bad matches (check left and right in the pattern to see if point matches given position)
for k,v in posfilter.items():
    i,j = k
    try:
        print(k, ''.join([str(k) for k in P[i][j]]))
        P_sx = ''.join([str(k) for k in P[i][j-1]])
        P_dx = ''.join([str(k) for k in P[i][j+1]])
        print("sx: ", P_sx)
        print("dx: ", P_dx)
    except:
        print("border")
    posfilter[k] = []
    for gp in v:
        print("----"*6)
        print(gp, gp.codeword)
        print("sx:", [g.codeword for g in gp.sx])
        print("dx:", [g.codeword for g in gp.dx])
        if any([P_sx in g.codeword for g in gp.sx]) and any([P_dx in g.codeword for g in gp.dx]):
            posfilter[k].append(gp)


filtered_gridcouples= []

#cleanup data structures for pickle dump
for k,v in posfilter.items():
    i,j = k
    try:
        if len(v) == 1:
            for gc in detected_gridcouples:
                if gc[0].position == v[0].position:
                    v[0].codeword = ''.join([str(i) for i in P[i][j]])
                    v[0].sx = []
                    v[0].dx = []
                    gc[1].codeword = v[0].codeword
                    filtered_gridcouples.append([v[0],gc[1]])
                    
            # v[0].VisualizeFinalCodeword(''.join([str(i) for i in P[i][j]]))
    except Exception as e:
        print(e)
        print(f"no points found on {i,j}" )

    
with open("non-coplanar1_codeword.dump", "wb") as pk:
    pickle.dump(filtered_gridcouples, pk)
