import numpy as np
from functools import reduce
from math import sqrt
import matplotlib.cm as cm
import matplotlib.pyplot as plt


def getWindow(coord, dims, img, square=True):
    x,y= coord
    xdim, ydim = dims
    xaxis = range(-xdim, ydim)
    yaxis = range(-ydim, ydim)
    return [[img[y+j][x+i] for i in xaxis] for j in yaxis]
        

def Filter(img, points, dims, logger=False):
    xdim, ydim = dims
    rr = np.copy(img)
    for x,y in points.keys():
        window=getWindow((x,y), dims, rr)
        threshold = max([max(i) for i in window])
        rr_show = np.copy(rr)
        if logger:
            print(f"window around pixel {x,y}")
            print(f"max value found in window {threshold}")
        for i in range(-xdim,xdim):
            for j in range(-ydim,ydim):
                if rr[y+j][x+i] < threshold:
                    rr[y+j][x+i] = 0 
                elif logger and i == -xdim or i == xdim or j == -ydim or j == ydim:
                    rr_show[y+j][x+i] = 1
        if logger:
            rr_show = [[rr_show[y+j][x+i] for i in range(-64,64)] for j in range(-64,64)]
            Confront(rr_show, window)
    return rr


def CrossConvolution(capture, size, normalize=True):
    convolved_capture = np.zeros(capture.shape, dtype="float64")
    
    for i in range(size,len(capture[:-size])):
        for j in range(size,len(capture[0][:-size])):
            first_term = sum([capture[(i+k)%len(capture)][j] for k in range(-size,size)])
            second_term = sum([capture[i][(j+k)%len(capture[0])] for k in range(-size,size)])
            convolved_capture[i][j]= abs(first_term - second_term)
    return Normalize(convolved_capture) if normalize else convolved_capture 

def TwofoldSymmetryRotationExtended(cpt, size, indexes, normalize=True, rectify=True):
    cpt = np.array(cpt, dtype="float64")
    def computeConvolution(x,y):
        window = [[cpt[(y+i)%len(cpt)][(x+j)%len(cpt[0])] for i in range(-size,size)] for j in range(-size,size)]
        rotwindow = [i[::-1] for i in window][::-1]
        
        # linearize matrices
        window=reduce(lambda x,y: x+y, window)
        rotwindow=reduce(lambda x,y: x+y,rotwindow)

        # calculate mean for both windows
        mean_window = sum(window)/len(window)
        mean_rotwindow = sum(rotwindow)/len(rotwindow)

        # build terms
        # num=len(window)*sum([window[i]*rotwindow[i] for i in range(len(window))]) - sum(window) * sum(rotwindow)
        num=sum([(window[i]-mean_window)*(rotwindow[i]-mean_rotwindow) for i in range(len(window))])
        first_term_den=sum([(window[i]-mean_window)**2 for i in range(len(window))])
        # first_term_den=sqrt(len(window)*sum([(window[i])**2 for i in range(len(window))]) - sum(window[i] for i in range(len(window)))**2)
        second_term_den=sum([(rotwindow[i]-mean_rotwindow)**2 for i in range(len(rotwindow))])
        # second_term_den=sqrt(len(rotwindow)*sum([(rotwindow[i])**2 for i in range(len(rotwindow))]) - sum(rotwindow[i] for i in range(len(rotwindow)))**2)
        # compute final result
        return  num / sqrt(first_term_den * second_term_den)
    convolved_capture = np.zeros(cpt.shape, dtype="float32")
    for x,y in indexes:
        convolved_capture[y][x] = computeConvolution(x,y)
        # print(convolved_capture[y][x])
    # for y in range(len(cpt)):
    #     for x in range(len(cpt[0])):
    #         if (x,y) in indexes:
    #             convolved_capture[y][x] = computeConvolution(x,y)
    #         
    #         else:
    #             convolved_capture[y][x] = 0
    if normalize and rectify:
        return Normalize(Rectify(convolved_capture))
    elif normalize:
        return Normalize(convolved_capture)
    elif rectify:
        return Rectify(convolved_capture)
    else:
        return convolved_capture


def TwofoldSymmetryRotation(cpt, size, indexes, normalize=True, rectify=True):
    cpt = np.array(cpt, dtype="float64")
    def computeConvolution(x,y):
        window = [[cpt[(y+i)%len(cpt)][(x+j)%len(cpt[0])] for i in range(-size,size)] for j in range(-size,size)]
        rotwindow = [i[::-1] for i in window][::-1]
        
        # linearize matrices
        window=reduce(lambda x,y: x+y, window)
        rotwindow=reduce(lambda x,y: x+y,rotwindow)

        # calculate mean for both windows
        # mean_window = sum(window)/len(window)
        # mean_rotwindow = sum(rotwindow)/len(rotwindow)

        # build terms
        num=len(window)*sum([window[i]*rotwindow[i] for i in range(len(window))]) - sum(window) * sum(rotwindow)
        # num=sum([(window[i]-mean_window)*(rotwindow[i]-mean_rotwindow) for i in range(len(window))])
        # first_term_den=sum([(window[i]-mean_window)**2 for i in range(len(window))])
        first_term_den=sqrt(len(window)*sum([(window[i])**2 for i in range(len(window))]) - sum(window[i] for i in range(len(window)))**2)
        # second_term_den=sum([(rotwindow[i]-mean_rotwindow)**2 for i in range(len(rotwindow))])
        second_term_den=sqrt(len(rotwindow)*sum([(rotwindow[i])**2 for i in range(len(rotwindow))]) - sum(rotwindow[i] for i in range(len(rotwindow)))**2)
        # compute final result
        return  num / (first_term_den * second_term_den)
    convolved_capture = np.zeros(cpt.shape, dtype="float32")
    for x,y in indexes:
        convolved_capture[y][x] = computeConvolution(x,y)
        # print(convolved_capture[y][x])
    # for y in range(len(cpt)):
    #     for x in range(len(cpt[0])):
    #         if (x,y) in indexes:
    #             convolved_capture[y][x] = computeConvolution(x,y)
    #         
    #         else:
    #             convolved_capture[y][x] = 0
    if normalize and rectify:
        return Normalize(Rectify(convolved_capture))
    elif normalize:
        return Normalize(convolved_capture)
    elif rectify:
        return Rectify(convolved_capture)
    else:
        return convolved_capture


def Visualize(x):
    plt.figure()
    plt.imshow(x, cmap="gray")
    plt.show()

def Confront(x,y):
    fig = plt.figure()
    p1 = fig.add_subplot(121)
    p1.imshow(x, cmap="gray")
    p2 = fig.add_subplot(122)
    p2.imshow(y,cmap="gray")
    fig.colorbar(cm.ScalarMappable(cmap="gray"))
    plt.show()


def Normalize(img):
    return img/max([max(i) for i in img])

def Rectify(img):
    return abs(img)
    
