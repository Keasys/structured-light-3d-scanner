import cv2 as cv
import numpy as np
import pickle
import matplotlib.pyplot as plt
from grid_points import GridPoint
import math

def getDistance(elem):
    def inner(x):
        return math.sqrt(sum([(elem.position[i]-x.position[i])**2 for i in range(len(x.position))]))
    return inner


src = "../pattern/pattern_test.png"

with open(f"./projector_indexes.dump", "rb") as pk:
    indexes = pickle.load(pk)
print(len(indexes))



img = cv.imread(src,0)
img = cv.cvtColor(img, cv.COLOR_GRAY2BGR)




gridpoints = [GridPoint(i,8, img) for i in indexes.keys()]

type_one_points = [p for p in gridpoints if p.type==0]
type_two_points = [p for p in gridpoints if p.type==1]
print(len(gridpoints))

for gp in gridpoints:
    pos = gp.position
    img = cv.circle(img, pos, 1, (255,0,0)) if gp.type == 0 else cv.circle(img, pos, 1, (0,0,255))


gridcouples = []

for t1 in type_one_points:
    x,y = t1.position
    dist = getDistance(t1)
    if len([(t2) for t2 in type_two_points if t2.position[0]>x and t2.position[1] < y]) < 1:
        continue
    t2 = min([t2 for t2 in type_two_points if t2.position[0]<x and t2.position[1] < y], key=dist)
    print(t2)
    img = cv.line(img, t1.position, t2.position, (134,19,113))
    gridcouples.append((t1, t2))


print(len(gridcouples))

plt.figure()
plt.imshow(img)
plt.show()

img = cv.imread(src)

for t1, t2 in gridcouples:
    t1.computeCodeWordProjectorPattern(type_one_points, img)
    t2.codeword = t1.codeword


with open("../projector_point_codewords.dump", "wb") as pk:
    pickle.dump(gridcouples, pk)

