import glob
import pickle

from grid_points import GridPoint
import cv2 as cv
import pickle
from math import exp

from time import time
from detector import *

import random
import sys


import tkinter as tk
from tkinter import ttk, Canvas
import cv2
from PIL import Image, ImageTk



src = sys.argv[1]
name=src.split("/")[-1][:-4]

original = cv.imread(src)
gray = cv.cvtColor(original, cv.COLOR_BGR2GRAY)
displayed_image = original
current_indexes = None
cross_indexes = None
width, height = 1500, 800

root = tk.Tk()
root.geometry(f"{width}x{height}")

lmain = tk.Label(root)


print(original.shape, original.dtype)


def getMap():
    global current_indexes
    char_matrix = [[1 if (j,i) in current_indexes else 0 for i in range(shape[1])] for j in range(shape[0])]
    return char_matrix

def onDump():
    global current_indexes
    with open(f"{name}_indexes.dump", "wb") as pk:
        pickle.dump(current_indexes, pk)
    print("wrote to file")


def onCrossThresholdAdjust(event):
    global displayed_image, cross_indexes
    cross_threshold_value.configure(text=f"{cross_threshold.get():.2f}")
    if cross_indexes is None: return
    cross_indexes = {(i,j):displayed_image[j][i] for j in range(len(displayed_image)) for i in range(len(displayed_image[0])) if displayed_image[j][i] > cross_threshold.get()}
    print("threshold adjusted: {len(current_indexes)}")
    monitor = np.array(displayed_image, dtype="uint8")
    if len(monitor.shape) < 3:
        monitor = cv.cvtColor(monitor, cv.COLOR_GRAY2BGR)
    color = (255,0,0)
    for x,y in cross_indexes:
        monitor[y][x][0]=255
        monitor[y][x][1]=0
        monitor[y][x][2]=0
    img = Image.fromarray(monitor)
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def onCurrentThresholdAdjust(event):
    global displayed_image, current_indexes
    twofold_threshold_value.configure(text=f"{twofold_threshold.get():.2f}")
    if current_indexes is None: return
    current_indexes = {(i,j):displayed_image[j][i] for j in range(len(displayed_image)) for i in range(len(displayed_image[0])) if displayed_image[j][i] > twofold_threshold.get()}
    print("threshold adjusted: {len(current_indexes)}")
    monitor = np.array(displayed_image, dtype="uint8")
    if len(monitor.shape) < 3:
        monitor = cv.cvtColor(monitor, cv.COLOR_GRAY2BGR)
    color = (255,0,0)
    for x,y in current_indexes:
        monitor[y][x][0]=255
        monitor[y][x][1]=0
        monitor[y][x][2]=0
    img = Image.fromarray(monitor)
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def dumpPoints():
    pass

def onCrossConvolution():
    global displayed_image, cross_indexes
    displayed_image = CrossConvolution(gray, int(cross_wsize.get()))
    cross_indexes = {(i,j):displayed_image[j][i] for j in range(len(displayed_image)) for i in range(len(displayed_image[0])) if displayed_image[j][i] > cross_threshold.get()}
    img = Image.fromarray(np.array(displayed_image*255))
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk
    print(displayed_image.shape, displayed_image.dtype)

def onTwofold():
    global displayed_image, current_indexes, cross_threshold
    displayed_image =  TwofoldSymmetryRotation(gray,int(twofold_wsize.get()), cross_indexes.keys())
    print(displayed_image)
    print("done")
    current_indexes = {(i,j):displayed_image[j][i] for j in range(len(displayed_image)) for i in range(len(displayed_image[0])) if displayed_image[j][i] > twofold_threshold.get()}
    print(len(current_indexes))
    img = Image.fromarray(np.array(displayed_image*255))
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def onShowPoints():
    global displayed_image, current_indexes
    print(len(current_indexes))
    displayed_image = cv.imread(src)
    for x,y in current_indexes.keys():
        color = (200,0,180)
        displayed_image = cv.circle(original, (x,y), 1, color,1)
    img = Image.fromarray(np.array(displayed_image, dtype='uint8'))
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk

def onFilter():
    global displayed_image, current_indexes
    print(len(current_indexes))
    displayed_image= Filter(displayed_image,current_indexes,(int(filter_size.get()),int(filter_size.get())))
    current_indexes = {(i,j):displayed_image[j][i]   for j in range(len(displayed_image)) for i in range(len(displayed_image[0])) if displayed_image[j][i] != 0}
    print(len(cross_indexes))
    img = Image.fromarray(np.array(displayed_image*255, dtype='uint8'))
    imgtk = ImageTk.PhotoImage(image=img)
    frame.itemconfig(image_frame, image=imgtk)
    frame.image = imgtk
    

root.columnconfigure(0,weight=1)
root.columnconfigure(1,weight=1)
root.columnconfigure(2,weight=1)
root.columnconfigure(3,weight=3)
img = Image.fromarray(displayed_image)
imgtk = ImageTk.PhotoImage(image=img)

frame = tk.Canvas(root, width=width/2, height=height, scrollregion=(0,0,300,700))
image_frame = frame.create_image(width//3,height//2, image= imgtk)
scroll_bar = tk.Scrollbar(root)
show_points_button = ttk.Button(
    root, 
    text="Show Points", 
    command=onShowPoints
)
cross_convolution_button = ttk.Button(
    root, 
    text="Cross Convolution", 
    command=onCrossConvolution
)
filter_button = ttk.Button(
    root,
    text="Local Maxima",
    command=onFilter
)

cross_wsize_label = ttk.Label(root, text="window size")
cross_threshold_label = ttk.Label(root, text="threshold")
twofold_threshold_label = ttk.Label(root, text="threshold")
twofold_wsize_label = ttk.Label(root, text="window size")
filter_size_label = ttk.Label(root, text="window size")

cross_threshold_value = ttk.Label(root, text="0")
cross_wsize_value = ttk.Label(root, text="0")
twofold_threshold_value = ttk.Label(root, text="0")
twofold_wsize_value = ttk.Label(root, text="0")
filter_size_value = ttk.Label(root, text="0")

cross_wsize = ttk.Scale(
    root,
    from_=1,
    to=24,
    orient='horizontal',  # horizontal
    command = lambda x: cross_wsize_value.configure(text=f"{cross_wsize.get():.2f}")
)

twofold_wsize = ttk.Scale(
    root,
    from_=1,
    to=24,
    orient='horizontal',  # horizontal,
    command = lambda x: twofold_wsize_value.configure(text=f"{twofold_wsize.get():.2f}")
)

cross_threshold = ttk.Scale(
    root,
    from_=0,
    to=1,
    orient='horizontal',  # horizontal
    command = onCrossThresholdAdjust
)

twofold_threshold = ttk.Scale(
    root,
    from_=0,
    to=1,
    orient='horizontal',  # horizontal
    command = onCurrentThresholdAdjust
)
filter_size = ttk.Scale(
    root,
    from_=0,
    to=12,
    orient='horizontal',
    command = lambda x: filter_size_value.configure(text=f"{filter_size.get():.2f}")
)

twofold_symmetry_button = ttk.Button(
    root, 
    text="TwoFold Symmetry Rotation", 
    command=onTwofold
)


dump_button = ttk.Button(
    root, 
    text="Dump", 
    command=onDump
)


dump_button.grid(
    column=2,
    row=9
)

filter_button.grid(
    column=2,
    row=7
)
filter_size.grid(
    column=2,
    row=8
)

frame.grid(
    column=3,
    row=0,
    rowspan=12,
    sticky='w'
    
)

scroll_bar.grid(
    column=4,
    row=0,
    rowspan=4
)


show_points_button.grid(
    column=2,
    row=0,
    sticky='we'
)


cross_convolution_button.grid(
    column=2,
    row=1,
    sticky='we'
)


cross_wsize.grid(
    column=2,
    row=2,
    sticky='we'
)




cross_threshold_value.grid(column=1, row=3) 
cross_wsize_value.grid(column=1, row=2)
twofold_threshold_value.grid(column=1, row=6)
twofold_wsize_value.grid(column=1, row=5)


cross_wsize_label.grid(column=0, row=2)
cross_threshold_label.grid(column=0, row=3)


cross_threshold.grid(
    column=2,
    row=3,
    sticky='we'
)


twofold_symmetry_button.grid(
    column=2,
    row=4,
    sticky='we'
)
twofold_threshold.grid(
    column=2,
    row=6,
    sticky='we'
)
twofold_wsize.grid(
    column=2,
    row=5,
    sticky='we'
)
filter_size_value.grid(column=1, row=8)
twofold_wsize_label.grid(column=0, row=5)
twofold_threshold_label.grid(column=0, row=6)
filter_size_label.grid(column=0, row=8)
scroll_bar.config( command = frame.yview )

frame.config( yscrollcommand=scroll_bar.set )

def getManhattanDistance(x, ref="p1top2"):
    p,q = x
    p_x, p_y = p.position
    q_x, q_y = q.position
    manhattan = p_x - q_x,  p_y - q_y if ref == "p1top2" else q_x - p_x,  q_y - p_y 
    return 5000 if manhattan[0] < 0 or manhattan[1] < 0 else manhattan[0] + manhattan[1]

root.mainloop()
sys.exit()






        

































print(displayed_image.dtype)
Visualize(displayed_image)

name=src[6:-4]
print(f"loading picture {name}")


# CROSS CONVOLUTION - FIND GRID POINTS HYPOTHESIS

x = time()
cc = CrossConvolution(gray, 3)
delta_t = time() - x



# cc=cc**2
indexes = {(i,j):cc[j][i] for j in range(len(cc)) for i in range(len(cc[0])) if cc[j][i] > 0.3}    
hyp_points_no = len(indexes)

print(f"found {hyp_points_no} hypothetic grid points on {sum([len(i) for i in gray])}")
print(f"elapsed time: {delta_t}")
Visualize(cc)

# TWOFOLD SYMMETRY ROTATION - FIND TRUE CANDIDATE
    
x = time()
rr = TwofoldSymmetryRotation(gray,3, indexes.keys())
delta_t = time() - x

indexes = {(i,j):rr[j][i]   for j in range(len(rr)) for i in range(len(rr[0])) if abs(rr[j][i]) != 0 }    
candidate_points_no=len(indexes)

print(f"elapsed time: {delta_t}")
Visualize(rr)
        
# FIND LOCAL MAXIMA - MAX IN SLIDING WINDOW

x = time()
rr_filtered= filter(rr,indexes,(3,3))
delta_t = time() - x

indexes = {(i,j):rr_filtered[j][i]   for j in range(len(rr_filtered)) for i in range(len(rr_filtered[0])) if rr_filtered[j][i] != 0}
true_points_no = len(indexes)

print(f"found {true_points_no} true grid points on {sum([len(i) for i in gray])}")
print(f"elapsed time: {delta_t}")
Visualize(rr_filtered)

original = cv.imread(src)
for x,y in indexes.keys():
    color = tuple(np.random.randint(0,255,3).tolist())
    original = cv.circle(original, (x,y), 1, color,1)
cv.imwrite(f"{name}_detected_points.png", original)
print("wrote to file")


Visualize(original)

print("computing grid points")
original = cv.imread(src)
gridpoints = [GridPoint(i,4, original) for i in indexes.keys()]

type_one_points = [p for p in gridpoints if p.type==0]
type_two_points = [p for p in gridpoints if p.type==1]
 
    
print(f"print type one: {len(type_one_points)}\ttype two: {len(type_two_points)}")
    
    
gridcouples = [min([(p,q) for q in type_two_points], key=getManhattanDistance) for p in type_one_points]
     

for p,q in gridcouples:
    cv.line(original, pt1=p.position, pt2=q.position, color=(255,170,0),thickness=1)

plt.figure()
plt.imshow(original)
plt.show()

gridpoints = []
for p,q in gridcouples:
    p.computeCodeWord(type_one_points, original)
    q.codeword = p.codeword
    gridpoints.append(p)
    gridpoints.append(q)




    
