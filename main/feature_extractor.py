import glob
import pickle

from grid_points import GridPoint
import cv2 as cv
import pickle


from time import time
from detector import *

import random

img = cv.imread("./img/io_crop.jpg", 0)
plt.figure()
plt.imshow(img, cmap="gray")
plt.show()

def getManhattanDistance(x, ref="p1top2"):
    p,q = x
    p_x, p_y = p.position
    q_x, q_y = q.position
    manhattan = p_x - q_x,  p_y - q_y if ref == "p1top2" else q_x - p_x,  q_y - p_y 
    return 5000 if manhattan[0] < 0 or manhattan[1] < 0 else manhattan[0] + manhattan[1]

def detectionProcedure(src, interactive=False):
    name=src[6:-4]
    print(f"loading picture {name}")

    original = cv.imread(src)
    gray = cv.cvtColor(original, cv.COLOR_BGR2GRAY)

    # CROSS CONVOLUTION - FIND GRID POINTS HYPOTHESIS
    x= time()
    cc = CrossConvolution(gray, 5)
    delta_t = time() - x

    indexes = {(i,j):cc[j][i] for j in range(len(cc)) for i in range(len(cc[0])) if cc[j][i] > 0.4}    
    hyp_points_no = len(indexes)

    print(f"found {hyp_points_no} hypothetic grid points on {sum([len(i) for i in gray])}")
    print(f"elapsed time: {delta_t}")
    if interactive:
        Confront(gray, cc)

    #LOCAL MAXIMA

    x = time()
    cc_filtered = Filter(cc, indexes, (5,5))
    delta_t = time() - x


    print(f"elapsed time: {delta_t}")
    if interactive:
        Visualize(cc_filtered)
    indexes = {(i,j):cc_filtered[j][i]   for j in range(len(cc_filtered)) for i in range(len(cc_filtered[0])) if cc_filtered[j][i] > 0.1}

    original = cv.imread(src, 0)
    original = cv.cvtColor(original, cv.COLOR_GRAY2BGR)
    for x,y in indexes.keys():
        original = cv.circle(original, (x,y), 1, (0,0,255),1)
    cv.imwrite(f"{name}_local_maxima_points.png", original)
    print("wrote to file")


    # TWOFOLD SYMMETRY ROTATION - FIND TRUE CANDIDATE
    
    x = time()
    rr = TwofoldSymmetryRotationExtended(gray,4, indexes.keys())
    delta_t = time() - x

    indexes = {(i,j):rr[j][i]   for j in range(len(rr)) for i in range(len(rr[0])) if abs(rr[j][i]) > 0.6 }    
    candidate_points_no=len(indexes)

    print(f"elapsed time: {delta_t}")
    if interactive:
        Visualize(rr)

    # FIND LOCAL MAXIMA - MAX IN SLIDING WINDOW

    x = time()
    rr_filtered= Filter(rr,indexes,(5,5))
    delta_t = time() - x

    indexes = {(i,j):rr_filtered[j][i]   for j in range(len(rr_filtered)) for i in range(len(rr_filtered[0])) if rr_filtered[j][i] != 0}
    true_points_no = len(indexes)

    print(f"found {true_points_no} true grid points on {sum([len(i) for i in gray])}")
    print(f"elapsed time: {delta_t}")
    if interactive:
        Visualize(rr_filtered)

    original = cv.imread(src)
    for x,y in indexes.keys():
        original = cv.circle(original, (x,y), 2, (0,0,255),1)
    cv.imwrite(f"{name}_detected_points.png", original)
    print("wrote to file")

    return indexes

    
src = ""

scene_points = detectionProcedure(src, interactive=True)

random.shuffle(proj_points)

iter_proj=iter(proj_points)
p = next(iter_proj)

projector = cv.imread("./img/pattern.png")
scene = cv.imread(src)

scene_dict = {}
proj_dict = {}

scene_coordinates = []
projector_coordinates = []

for i in proj_points:
    if i.codeword == "border":
        continue
    scene_dict[tuple(i.codeword)] = []
    proj_dict[tuple(i.codeword)] = i
    for j in scene_points:
        if i == j:
            scene_dict[tuple(i.codeword)].append(j)
        


    
scene_dict_filtered = {k:v for k,v in scene_dict.items() if len(v) == 1}
shuffled_keys = list(scene_dict_filtered.keys())
random.shuffle(shuffled_keys)
shuffled_keys = shuffled_keys[:50]
print(len(shuffled_keys))
shuffled_keys_filtered = []


with open("scene.pkl", "wb") as pk:
    pickle.dump(scene_dict, pk)

